'''
Evaluate a model on natural examples
'''

from scipy.special import softmax
import torch
import torchvision
import numpy as np
import json
import sys
from train import cifar_stats
from utils import print_performance

def evaluate(dataset=None, model_path=None):
    if dataset == 'mnist':
        test_dataset = torchvision.datasets.MNIST(
            root='./mnist', train=False, download=False,
            transform=torchvision.transforms.ToTensor()
        )
    elif dataset == 'cifar10':
        transform_test = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize(*cifar_stats)
        ])
        test_dataset = torchvision.datasets.CIFAR10(
            root='./cifar10', train=False, download=False,
            transform=transform_test,
        )
    else:
        raise ValueError('Unsupported dataset: ' + str(dataset))
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=100, shuffle=False)
    model = torch.load(model_path, map_location=torch.device('cpu'))
    print('Evaluating %s on %s (clean)' % (model_path, dataset), file=sys.stderr)

    num_correct = num_rejected = total = 0
    for x, y in test_loader:
        preds, _ = model(x)
        preds = preds.detach().numpy()
        preds_softmax = softmax(preds, axis=1)
        batch_max_probs = np.max(preds_softmax, axis=1)
        rejected = (batch_max_probs < 0.5)
        num_rejected += rejected.sum()
        num_correct += (np.argmax(preds, axis=1) == y.numpy())[~rejected].sum()
        total += len(y)

    accuracy, rejection_ratio = print_performance(num_correct, num_rejected, total)
    results = {
        'model_path': model_path,
        'attack': 'none',
        'accuracy': float(accuracy),
        'rejection_ratio': float(rejection_ratio),
    }
    print(json.dumps(results))

if __name__ == "__main__":
    import fire
    fire.Fire(evaluate)