from itertools import zip_longest
import sys

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def grouper_variable_length(iterable, n):
    "Collect data into variable-length chunks or blocks"
    # grouper('ABCDEFG', 3) --> ABC DEF G"
    block = []
    for val in iterable:
        block.append(val)
        if len(block) >= n:
            yield block
            block = []
    if len(block) >= 1:
        yield block

def print_performance(num_correct, num_rejected, total):
    num_predicted = total-num_rejected
    accuracy = num_correct/num_predicted if num_predicted else float('nan')
    rejection_ratio = num_rejected/total
    print('accuracy: %.4f (%d/%d), reject: %.4f (%d/%d)'
            %(accuracy, num_correct, num_predicted,
            rejection_ratio, num_rejected, total), file=sys.stderr)
    return accuracy, rejection_ratio
